Ticketee::Application.routes.draw do
  default_url_options :host => "localhost:3000"

  devise_for :users, controllers: {registrations: 'registrations'}
  devise_scope :user do
    get '/signin' => 'devise/sessions#new'
    post '/signin' => 'devise/sessions#create'
    delete '/signout' => 'devise/sessions#destroy'
    get '/signup' => 'devise/registrations#new'
  end
  resources :users, :only => :show
  resources :projects do
    resources :tickets do
      collection do
        get :search
      end
      member do
        post :watch
      end
    end
  end
  resources :tickets do
    resources :comments
    resources :tags do
      member do
        delete :remove
      end

    end

  end
  resources :files

  namespace :admin do
    root :to => "base#index"
    resources :users do
      resources :permissions
      put "permissions", to: "permissions#set", as: "set_permissions"
    end
    resources :states do
      member do
        get :make_default
      end
    end

  end

  namespace :api do
    namespace :v1 do
      resources :projects do
        resources :tickets
      end
    end
    namespace :v2 do
      resources :projects do
        resources :tickets
      end
    end
  end

  root 'projects#index'
end
