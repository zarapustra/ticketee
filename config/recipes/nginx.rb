namespace :nginx do
  on roles :web do
    desc "Install latest stable release of nginx"
    task :install do
      run sudo "add-apt-repository ppa:nginx/stable"
      run sudo "apt-get -y update"
      run sudo "apt-get -y install nginx"
    end
    after "deploy:install", "nginx:install"

    desc "Setup nginx configuration for this application"
    task :setup do
      template "nginx_unicorn.sh.erb", "/tmp/nginx_conf"
      run sudo "mv /tmp/nginx_conf /etc/nginx/sites-enabled/#{fetch :application}"
      run sudo "rm -f /etc/nginx/sites-enabled/default"
      restart
    end
    after "deploy:setup", "nginx:setup"

    desc "#{command} nginx"
    %w[start stop restart].each do |command|
      task command do
        run sudo "service nginx #{command}"
      end
    end
  end
end

# NOTE: I found it necessary to manually fix the init script as shown here
# https://bugs.launchpad.net/nginx/+bug/1033856
