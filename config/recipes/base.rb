=begin
def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(key, value=nil, &block)

  set(key, value, &block) unless fetch(key)==value
end

namespace :deploy do
  desc "Install everything onto the server"
  task :install do
    on roles :app do
      run sudo "apt-get -y update"
      run sudo "apt-get -y install python-software-properties curl git-core"
    end
  end
end
=end
