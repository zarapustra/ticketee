set_default(:unicorn_user) { fetch(:user) }
set_default(:unicorn_pid) { "#{fetch :shared_path}/tmp/pids/unicorn.pid" }
set_default(:unicorn_config) { "#{fetch :shared_path}/config/unicorn.rb" }
set_default(:unicorn_log) { "#{fetch :shared_path}/log/unicorn.log" }
set_default(:unicorn_workers, 2)

namespace :unicorn do
  on roles :app do
    desc 'Setup Unicorn initializer and app configuration'
    task :setup, roles: :app do
      run "mkdir -p #{fetch :shared_path}/config"
      template 'unicorn.rb.erb', fetch :unicorn_config
      template 'unicorn_init.sh.erb', '/tmp/unicorn_init'
      run 'chmod +x /tmp/unicorn_init'
      run "#{sudo} mv /tmp/unicorn_init /etc/init.d/unicorn_#{fetch :application}"
      run "#{sudo} update-rc.d -f unicorn_#{fetch :application} defaults"
    end
    after 'deploy:setup', 'unicorn:setup'

    %w[start stop restart].each do |command|
      desc "#{command} unicorn"
      task command, roles: :app do
        run "service unicorn_#{fetch :application} #{command}"
      end
      after "deploy:#{command}", "unicorn:#{command}"
    end
  end
end
