set_default(:postgresql_host, "localhost")
set_default(:postgresql_user) { application }
set_default(:postgresql_password) { Capistrano::CLI.password_prompt "PostgreSQL Password: " }
set_default(:postgresql_database) { "#{application}_production" }

namespace :postgresql do
  on roles :db, only: {primary: true} do
    desc "Install the latest stable release of PostgreSQL."
    task :install do
      run sudo "add-apt-repository ppa:pitti/postgresql"
      run sudo "apt-get -y update"
      run sudo "apt-get -y install postgresql libpq-dev"
    end
    after "deploy:install", "postgresql:install"

    desc "Create a database for this application."
    task :create_database do
      run sudo %Q{-u postgres psql -c "create user #{fetch :postgresql_user} with password '#{fetch :postgresql_password}';"}
      run sudo %Q{-u postgres psql -c "create database #{fetch :postgresql_database} owner #{fetch :postgresql_user};"}
    end
    after "deploy:setup", "postgresql:create_database"
  end
  on roles :app do
    desc "Generate the database.yml configuration file."
    task :setup do
      run "mkdir -p #{fetch :shared_path}/config"
      template "database.yml.erb", "#{fetch :shared_path}/config/database.yml"
    end
    after "deploy:setup", "postgresql:setup"

    desc "Symlink the database.yml file into latest release"
    task :symlink do
      run "ln -nfs #{fetch :shared_path}/config/database.yml #{fetch :release_path}/config/database.yml"
    end
    after "deploy:finalize_update", "postgresql:symlink"
  end
end
