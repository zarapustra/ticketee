# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'ticketee'
set :user, "deploy"
set :repo_url, 'git@bitbucket.org:zarapustra/ticketee.git'
set :deploy_to, "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :keep_releases, 5
#set :branch, 'master'
set :log_level, :info
#set :tty, true
set :sudo, true

set :linked_files, %w[config/initializers/mail.rb config/database.yml]
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/uploads}

# Ruby свистелки
set :rbenv_type, :user
set :rbenv_ruby, '2.2.3'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_roles, :all

# А это рекомендуют добавить для приложений, использующих ActiveRecord
set :puma_init_active_record, true

