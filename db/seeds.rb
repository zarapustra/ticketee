if (ActiveRecord::Base.connection.table_exists? 'users')

  User.create(email: "admin@example.com",
      name: "admin",
      password: "password",
      password_confirmation: "password",
      admin: true)
end
if (ActiveRecord::Base.connection.table_exists? 'projects')

  Project.create(name: "Ticketee Beta")
end
if (ActiveRecord::Base.connection.table_exists? 'states')

  State.create(:name => "New", :background => "#85FF00", :color => "white", :default => true)
  State.create(:name => "Open", :background => "#00CFFD", :color => "white")
  State.create(:name => "Closed", :background => "black", :color => "white")
end