require 'spec_helper'
feature "Profile page" do
  scenario "viewing" do
    user = FactoryGirl.create(:user)
    sign_in_as!(user)
    visit user_path(user)
    expect(page).to have_content(user.name)
    expect(page).to have_content(user.email)
  end
end
feature "Editing Users" do
  scenario "Updating a profile without changing password" do
    user = FactoryGirl.create(:user)
    sign_in_as!(user)
    visit user_path(user)
    click_link "Edit Profile"
    expect(page).to have_content("Name")
    fill_in "Name", with: "Kim Chen In"
    fill_in "Current password", with: user.password
    click_button "Update"
    expect(page).to have_content("Profile has been updated.")
  end
end
