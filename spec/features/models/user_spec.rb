require 'spec_helper'
describe User do
  describe "passwords" do
    it "needs a password and confirmation to save" do
      u = User.new(name: "steve", email: "steve@example.com")
      u.save
      expect(u).to_not be_valid
      u.password = "password"
      u.password_confirmation = ""
      u.save
      expect(u).to_not be_valid
      u.password_confirmation = "password"
      u.save
      expect(u).to be_valid
    end
    it "needs password and confirmation to match" do
      u = User.create(
          name: "steve",
          email: "steve@example.com",
          password: "hunters2",
          password_confirmation: "hunterss")
      expect(u).to_not be_valid
    end
    it "requires an email" do
      u = User.new(name: "steve",
                   password: "hunters2",
                   password_confirmation: "hunters2")
      u.save
      expect(u).to_not be_valid
      u.email = "steve@example.com"
      u.save
      expect(u).to be_valid
    end
    it "resets user request count" do
      user = Factory(:user)
      user.update_attribute(:request_count, 42)
      User.reset_request_count!
      user.reload
      expect(user.request_count).to eql(0)
    end

  end
  describe "authentication" do
    let(:user) { User.create(
        name: "steve",
        email: "steve@example.com",
        password: "hunters2",
        password_confirmation: "hunters2") }
    it "authenticates with a correct password" do
      user.password="hunters2"
      sign_in_as!(user)
      expect(page).to have_content('Signed in successfully.')
    end
    it "does not authenticate with an incorrect password" do
      visit '/signin'
      fill_in "Email", with: user.email
      fill_in "Password", with: "hunters1"
      click_button 'Sign in'
      expect(page).to have_no_content('Signed in successfully.')
    end
  end
end
