require "spec_helper"
feature "Ticket Notifications" do
  let!(:author) { FactoryGirl.create(:user, :name => "author") }
  let!(:commenter) { FactoryGirl.create(:user, :name => "commenter") }
  let!(:project) { FactoryGirl.create(:project) }
  let!(:ticket) do
    FactoryGirl.create(:ticket,
                       :project => project,
                       :user => author)
  end
  before do
    ActionMailer::Base.deliveries.clear
    define_permission!(author, "view", project)
    define_permission!(commenter, "view", project)
    sign_in_as!(commenter)
    visit '/'
  end
  scenario "Ticket owner receives notifications about comments" do
    click_link project.name
    click_link ticket.title
    fill_in "comment_text", :with => "Is it out yet?"
    click_button "Create Comment"
    email = find_email!(author.email)
    subject = "[ticketee] #{project.name} - #{ticket.title}"
    expect(email.subject).to include(subject)
    click_first_link_in_email(email)
    within("#ticket h2") do
      expect(page).to have_content(ticket.title)
    end
  end
  scenario "Comment authors are automatically subscribed to a ticket" do
    click_link project.name
    click_link ticket.title
    fill_in "comment_text", :with => "Is it out yet?"
    click_button "Create Comment"
    expect(page).to have_content("Comment has been created.")
    find_email!(author.email)
    click_link "Sign out"
    reset_mailer
    sign_in_as!(author)
    click_link project.name
    click_link ticket.title
    fill_in "comment_text", :with => "Not yet!"
    click_button "Create Comment"
    expect(page).to have_content("Comment has been created.")
    find_email!(commenter.email)
    #find_email!(author.email)
  end

end
