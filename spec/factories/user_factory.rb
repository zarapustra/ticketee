FactoryGirl.define do
  sequence(:email) { |n| "user#{n}@example.com" }
  sequence(:name) { |n| "na#{n}me" }

  factory :user do
    name { generate(:name) }
    email { generate(:email) }
    password "password1"
    password_confirmation "password1"

    factory :admin_user do
      name "admin"
      email { generate(:email) }
      password "password"
      password_confirmation "password"
      admin true
    end
  end
end
