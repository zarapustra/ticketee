class User < ActiveRecord::Base
  acts_as_token_authenticatable
  devise :database_authenticatable, :registerable, :validatable
  has_many :permissions
  before_save :ensure_authentication_token

  def self.reset_request_count!
    update_all("request_count = 0", "request_count > 0")
  end

  def to_s
    "#{email} (#{admin? ? "Admin" : "User"})"
  end

  private



  # def ensure_authenticaion_token
 #   reset_authentication_token if authentication_token.blank?
 # end

 # def reset_authentication_token
 #   self.authentication_token = self.class.authentication_token
 # end


end
