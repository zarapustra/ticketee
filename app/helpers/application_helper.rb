module ApplicationHelper
  def title(*parts)
    unless parts.empty?
      content_for :title do
        (parts << "Ticketee").join(" - ")
      end
    end
  end

  def admins_only(&block)
    block.call if current_user.try(:admin?)
  end

  def authorized?(permission, thing, &block)
    block.call if can?(permission.to_sym, thing) ||
        current_user.try(:admin?)
  end

  def signed_in_as?(user)
    if !user_signed_in? and current_user!=user
      flash[:alert] = "You cannot do that."
      redirect_to root_path
    end
  end

  def authenticate_admin!
    if signed_in? and !current_user.try(:admin?)
      flash[:alert] = "You must be an admin to do that."
      redirect_to root_path
    end
  end

end
