class RegistrationsController < Devise::RegistrationsController

  def new
    super
  end

  def create
    super
  end

  def update
    if update_resource(current_user, user_params)
      flash[:notice] = "Profile has been updated."
      redirect_to resource
    else
      flash[:alert] = "Profile has not been updated."
      render :edit
    end
  end

=begin
def update_resource(resource, user_params)
    if user_params[:password].blank? and
        user_params[:password_confirmation].blank? and
        user_params[:current_password].blank?
      command="update_without_password"
    else
      command = "update"
    end

    if resource.send(command, user_params)
      flash[:notice] = "Profile has been updated."
      redirect_to current_user
    else
      flash[:alert] = "Profile has not been updated."
      render :edit
    end
  end

=end
  private

  def user_params
    params.require(:user).permit(:name,
                                 :email,
                                 :password,
                                 :password_confirmation,
                                 :current_password)
  end

end